import os
import numpy as np
import geopandas
import pandas as pd
import rasterio as rio


def tile_and_make_stats_from_map_raster(input_raster, output_gdf, extent, tile_size):
    """
    take an OSO classificaiton MAP in raster format and produce a tiling of the extent
    with statistic by class

    Parameters
    ----------
    input_raster
    output_gdf
    extent
    tile_size

    Returns
    -------

    """

    gdf = None
    with rio.open(input_raster) as src:

        width = src.width
        height = src.height

    return output_gdf
