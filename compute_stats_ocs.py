import os
import pandas as pd
import geopandas as gpd
from tqdm import tqdm
import json

PATH_TO_OCS = "/home/ign.fr/skhelifi/data/ocsge-samy/vector-gt"
OCCUPATION = "OCCUPATION_SOL.shp"
ZONE_CONSTRUITE = "ZONE_CONSTRUITE.shp"
PATH_TO_MAT_CONF = '/home/ign.fr/skhelifi/data/ocsge-samy/mat_conf'


def find_shape_file_by_dep(root):
    d = {}
    for root, dirs, files in os.walk(root, topdown=False):

        for name in files:

            if name == "OCCUPATION_SOL.shp":
                key = root.split("/")[-1].replace("-", "_")
                d[key] = {} if key not in d.keys() else d[key]
                d[key]["OCCUPATION_SOL"] = os.path.join(root, name)
                d[key]["ZONE_CONSTRUITE"] = os.path.join(root, "ZONE_CONSTRUITE.shp")
                d[key]["PVA"] = key.split("_")[-1]
                d[key]["DEP"] = key.split("_")[-2][1:]
                d[key]["OCS_ID"] = key

            if name == "EMPRISE.shp":
                key = root.split("/")[-2].replace("-", "_")
                d[key] = {} if key not in d.keys() else d[key]
                d[key]["EMPRISE"] = os.path.join(root, name)

    return d


def stack_dep(d, folder, key_data):
    gdf = None

    for key, value in tqdm(d.items()):
        shp = value[key_data]
        actu_gdf = gpd.read_file(shp)
        extra_cols = ["PVA", "DEP", "OCS_ID"]
        extra_vals = [value["PVA"], value["DEP"], value["OCS_ID"]]
        actu_gdf[extra_cols] = pd.DataFrame([extra_vals], index=actu_gdf.index)
        gdf = pd.concat([gdf, actu_gdf]) if gdf is not None else actu_gdf

    gdf.to_file(os.path.join(folder, key_data + '-global.gpkg'), driver="GPKG")

    return gdf


def create_conf_matrix(d):

    joined = None

    for k, v in tqdm(d.items()):

        shp = v['OCCUPATION_SOL']
        current = gpd.read_file(shp)
        emprise = gpd.read_file(v["EMPRISE"])
        emprise_area = emprise.geometry.area
        total_area = emprise_area.sum()
        bati = gpd.read_file(v["ZONE_CONSTRUITE"])
        bati_area = bati.geometry.area
        total_bati = bati_area.sum()
        current["area"] = current.geometry.area
        grouped = current.groupby(["CODE_CS", "CODE_US"]).sum("area")
        grouped["total_area"] = total_area
        grouped["total_bati"] = total_bati
        grouped["ID_PROD"] = k
        grouped["DEP"] = v["DEP"]
        grouped["PVA"] = v["PVA"]
        # print(grouped.head())
        joined = pd.concat([joined, grouped]) if joined is not None else grouped
        # print(joined)

    joined.to_csv(os.path.join(PATH_TO_MAT_CONF, "matrice_confusion_ocs" + ".csv"))


if __name__ == "__main__":

    with open('/home/ign.fr/skhelifi/data/ocsge-samy/data.json', 'r') as fp:

        data = json.load(fp)

    create_conf_matrix(data)
