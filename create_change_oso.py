import os
import numpy as np
import rasterio as rio
from tqdm import tqdm

PATH_TO_OSO = "/media/NAS/DETECT_CHANGE/DATA/oso"

first_date =  "OCS_2016_CESBIO.tif"
first_date_file = os.path.join(PATH_TO_OSO, first_date)
second_date = "OCS_2018_CESBIO.tif"
second_date_aligned = "OCS_2018_CESBIO_aligned_with_2016.tif"
second_date_file = os.path.join(PATH_TO_OSO, second_date)

dico_2018 = {
    "batis denses": 1,
    "batis diffus": 2,
    "zones ind et com": 3,
    "surfaces routes": 4,
    "colza": 5,
    "cereales à pailles": 6,
    "protéagineux": 7,
    "soja": 8,
    "tournesol": 9,
    "maïs": 10,
    "riz": 11,
    "tubercules/racines": 12,
    "prairies" : 13,
    "vergers": 14,
    "vignes": 15,
    "forets de feuillus": 16,
    "forets de coniferes": 17,
    "pelouses": 18,
    "landes ligneuses": 19,
    "surfaces minérales": 20,
    "plages et dunes": 21,
    "glaciers ou neiges": 22,
    "eau": 23,
    "autres": 255
}
inv_dico_2018 = {value: key for key, value in dico_2018.items()}
print(inv_dico_2018)

dico_2016 = {
    "culture ete": 11,
    "culture hiver": 12,
    "foret feuillus": 31,
    "foret coniferes": 32,
    "pelouses": 34,
    "landes ligneuses": 36,
    "urbain dense": 41,
    "urbain diffus": 42,
    "zones ind et com": 43,
    "surfaces routes": 44,
    "surfaces minerales": 45,
    "plages et dunes": 46,
    "eau": 51,
    "glaciers ou neige": 53,
    "prairies": 211,
    "vergers": 221,
    "vignes": 222,
    "autres": 255
}

dico_2018_to_2016 = {
    "batis denses": "urbain dense",
    "batis diffus": "urbain diffus",
    "zones ind et com": "zones ind et com",
    "surfaces routes": "surfaces routes",
    "colza": "culture hiver",
    "cereales à pailles": "culture hiver",
    "protéagineux": "culture hiver",
    "soja": "culture ete",
    "tournesol": "culture ete",
    "maïs": "culture ete",
    "riz": "culture ete",
    "tubercules/racines": "culture hiver",
    "prairies": "prairies",
    "vergers": "vergers",
    "vignes": "vignes",
    "forets de feuillus": "foret feuillus",
    "forets de coniferes": "foret coniferes",
    "pelouses": "pelouses",
    "landes ligneuses": "landes ligneuses",
    "surfaces minérales": "surfaces minerales",
    "plages et dunes": "plages et dunes",
    "glaciers ou neiges": "glaciers ou neige",
    "eau": "eau",
    "autres": "autres"
}

dico_2016_to_artif = {
    11: 0,
    12: 0,
    31: 0,
    32: 0,
    34: 0,
    36: 0,
    41: 1,
    42: 1,
    43: 1,
    44: 1,
    45: 0,
    46: 0,
    51: 0,
    53: 0,
    211: 0,
    221: 0,
    222: 0,
    255: 0
}

idx_2018_to_2016 = {key: dico_2016[dico_2018_to_2016[value]] for key, value in inv_dico_2018.items()}
print(idx_2018_to_2016)
diff_values = [max(i, j) - min(i, j) for i in dico_2016.values() for j in dico_2016.values() if i != j]
print(f" diff values: {diff_values}")
print(f" length of diff values: {len(diff_values)}")
print(f" length of conbination: {len(dico_2016.values()) * (len(dico_2016.values()) - 1)}")


def align_nomenclature():
    """

    :return:
    """
    k = np.array(list(idx_2018_to_2016.keys()))
    v = np.array(list(idx_2018_to_2016.values()))
    sidx = k.argsort()  # k,v from approach #1
    k = k[sidx]
    v = v[sidx]

    with rio.open(second_date_file) as src:

        # meta = src.meta
        profile = src.profile
        windows = src.block_windows(1)
        arr = [i for i in src.block_windows(1)]
        length = len(arr)

        with rio.open(os.path.join(PATH_TO_OSO, second_date_aligned), "w", **profile) as dst:

            for ji, window in tqdm(windows, total=length):

                band = src.read(1, window=window)
                idx = np.searchsorted(k, band.ravel()).reshape(band.shape)
                idx[idx == len(k)] = 0
                mask = k[idx] == band
                out = np.where(mask, v[idx], 0).astype(np.uint8)
                dst.write_band(1, out, window=window)


def merge_urban(f, f_out):

    with rio.open(f) as src:
        # meta = src.meta
        profile = src.profile
        windows = src.block_windows(1)
        arr = [i for i in src.block_windows(1)]
        length = len(arr)

        with rio.open(os.path.join(PATH_TO_OSO, f_out), "w", **profile) as dst:

            for ji, window in tqdm(windows, total=length):

                band = src.read(1, window=window)
                msk = np.logical_or(band == 42, band == 43)
                band[msk] = 41
                dst.write_band(1, band, window=window)


def merge_culture(f, f_out):

    with rio.open(f) as src:

        # meta = src.meta
        profile = src.profile
        windows = src.block_windows(1)
        arr = [i for i in src.block_windows(1)]
        length = len(arr)

        with rio.open(os.path.join(PATH_TO_OSO, f_out), "w", **profile) as dst:

            for ji, window in tqdm(windows, total=length):

                band = src.read(1, window=window)
                msk = band == 12
                band[msk] = 11
                dst.write_band(1, band, window=window)


def calculate_pixel_diff_artif_non_artif(f1, f2, f_out_1, f_out_2, f_out_diff):

    k = np.array(list(dico_2016_to_artif.keys()))
    v = np.array(list(dico_2016_to_artif.values()))
    sidx = k.argsort()  # k,v from approach #1
    k = k[sidx]
    v = v[sidx]

    with rio.open(f1) as src1:


        # meta = src.meta
        profile = src1.profile
        profile_diff = profile.copy()
        print(profile_diff.keys())
        # profile_diff["dtype"] = "int8"

        windows = src1.block_windows(1)
        arr = [i for i in src1.block_windows(1)]
        length = len(arr)

        with rio.open(f2) as src2:

            with rio.open(os.path.join(PATH_TO_OSO, f_out_1), "w", **profile) as dst1:

                with rio.open(os.path.join(PATH_TO_OSO, f_out_2), "w", **profile) as dst2:

                    with rio.open(os.path.join(PATH_TO_OSO, f_out_diff), "w", **profile_diff) as dst_diff:

                        for ji, window in tqdm(windows, total=length):

                            band1 = src1.read(1, window=window)
                            idx = np.searchsorted(k, band1.ravel()).reshape(band1.shape)
                            idx[idx == len(k)] = 0
                            mask1 = k[idx] == band1
                            out1 = np.where(mask1, v[idx], 0).astype(np.uint8)
                            dst1.write_band(1, out1, window=window)

                            band2 = src2.read(1, window=window)
                            idx = np.searchsorted(k, band2.ravel()).reshape(band1.shape)
                            idx[idx == len(k)] = 0
                            mask2 = k[idx] == band2
                            out2 = np.where(mask2, v[idx], 0).astype(np.uint8)
                            dst2.write_band(1, out2, window=window)
                            artif_non_artif = np.logical_and(out1 == 1, out2 == 0)
                            non_artif_artif = np.logical_and(out1 == 0, out2 == 1)
                            out3 = out2 * 0
                            out3[artif_non_artif] = 1
                            out3[non_artif_artif] = 2
                            out3 = out3.astype(np.uint8)
                            dst_diff.write_band(1, out3, window=window)

if __name__ == "__main__":

    # align_nomenclature()
    # merge_urban("/media/NAS/DETECT_CHANGE/DATA/oso/OCS_2016_CESBIO_urban_merged.tif", "OCS_2016_CESBIO_urban_merged_culture_merged.tif")
    # merge_culture("/media/NAS/DETECT_CHANGE/DATA/oso/OCS_2016_CESBIO_urban_merged.tif", "OCS_2016_CESBIO_urban_merged_culture_merged.tif")
    # merge_culture("/media/NAS/DETECT_CHANGE/DATA/oso/OCS_2018_CESBIO_aligned_with_2016_urban_merged.tif", "OCS_2018_CESBIO_aligned_with_2016_urban_merged_culture_merged.tif")
    calculate_pixel_diff_artif_non_artif("/media/NAS/DETECT_CHANGE/DATA/oso/OCS_2016_CESBIO_urban_merged_culture_merged.tif",
                                         "/media/NAS/DETECT_CHANGE/DATA/oso/OCS_2018_CESBIO_aligned_with_2016_urban_merged_culture_merged.tif",
                                         "oso_artif_2016_2.tif",
                                         "oso_artif_2018_2.tif",
                                         "oso_diff_artif_2016_2018_2.tif"
                                         )
